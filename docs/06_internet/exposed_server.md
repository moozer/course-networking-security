Exposing a server
==========================0


Using digital ocean, we will spin up a VM that is accessible from the internet, and see what is received from the internet.

## Spin up a droplet

1. Sign in to Digital ocean

    * Either signup using using my [affiliate link](https://m.do.co/c/76c31d57930c), og directly on digitalocean.com.

2. Create droplet by clicking the "create" button and select "Droplets". It will take you [here](https://cloud.digitalocean.com/droplets/new)

3. Select 
    
    * Distribution: Debian 11
    * Basic plan
    * Regular with ssd
    * Select the cheapest option ($5/month)
    * No extra block storage
    * Select region of your liking, e.g. Amsterdam
    * Default VPNC network (unused in this exercise)
    * Select SSH key for Authentication
    
        * Passwords should not even be considered an option.
        * create a key, if you don't already have one.

    * No additional options
    * 1 droplet only
    * set a hostname if yo ulike
    * no tags needed
    * "Create droplet"

4. Go to "droplets" in the navigator to the left

5. Copy the IP address of the new droplet

6. use `ssh root@<ip>` to acces the new droplet.


Try `ip a`, `ping`, `traceroute` and such to get a feel for the environment.


## lookup at traffic

1. Issue an `ss -antp` on the VM

2. Make a note of the IP address you connect from

3. Start tcpdump: `tcpdump -n -i eth0 host not <ip>`

    * the extra filter is to avoid seeing our own SSH traffic.

4. Look at the traffic.

    * what leaves?
    * what comes in?
    * Any unsolicited?

## ssh

Depending on the system, you may have a lot of ssh brute force attacks.

It is seen by either in `tcpdump` by the amount of connections to port 22, og by looking into `/var/log/auth.log`

A failed login attempt - Invalid user:
```
Apr 19 12:17:12 netsec-ssh-test-fra sshd[1075]: Invalid user cirros from 211.36.141.239 port 15889
Apr 19 12:17:12 netsec-ssh-test-fra sshd[1075]: Received disconnect from 211.36.141.239 port 15889:11: Bye Bye [preauth]
Apr 19 12:17:12 netsec-ssh-test-fra sshd[1075]: Disconnected from invalid user cirros 211.36.141.239 port 15889 [preauth]
```

Login failed of known user. Probably trying a password on a keys-only login.
```
Apr 19 12:16:18 netsec-ssh-test-fra sshd[1024]: Received disconnect from 211.36.141.239 port 36477:11: Bye Bye [preauth]
Apr 19 12:16:18 netsec-ssh-test-fra sshd[1024]: Disconnected from authenticating user root 211.36.141.239 port 36477 [preauth]
```

Failed login user after a couple of hours
```
cat /var/log/auth.log | grep "Invalid user" | cut -d ' ' -f8 | sort | uniq | tr '\n' ','
admin,alarm,cirros,debian,ethos,guest,mine,miner,nagios,oracle,pi,postgres,soporte,support,test,tomcat,ubnt,ubuntu,user,vagrant,volumio,xbmc,
```

Note that "root" is not in the list, since it is a valid user on the system.

This matches [a top-10 list from 2010](https://yurisk.info/2010/06/04/top-10-usernames-used-in-ssh-brute-force/).

Failing to establish key exchange

```
Apr 19 12:15:39 netsec-ssh-test-fra sshd[994]: Unable to negotiate with 61.177.173.40 port 42524: no matching key exchange method found. Their offer: diffie-hellman-group1-sha1,diffie-hellman-group14-sha1,diffie-hellman-group-exchange-sha1 [preauth]
```

Notice the `-sha1`. That should not be used anymore. See [RFC9142](https://datatracker.ietf.org/doc/rfc9142/) for details.

We are running `debian/bullseyes` and default behavior is to disable old key exchanges. 


For reference, a successful login looks like this

```
Apr 19 12:45:20 netsec-ssh-test-fra sshd[1211]: Accepted publickey for root from <my ip> port 36255 ssh2: ED25519 SHA256:<my pub fingerprint>
Apr 19 12:45:20 netsec-ssh-test-fra sshd[1211]: pam_unix(sshd:session): session opened for user root(uid=0) by (uid=0)
```