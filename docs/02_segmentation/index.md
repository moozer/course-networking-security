
Segmentation
=======================

When securing data and systems, how data flow is important. If the adversary cannot reach the data, then security is much easier.

Data in transit is always in risk of being intercepted, and segmenting the network is a key component to controlling which data is flowing where. It is also an optimization issue, where some systems will communicate a lot with each other, and hence should network wise be place close to each other.

Segmentation comes in multiple flavors from layer 2 with switching and VLANS, to VPNs to do site-to-site logical tunnels and routing (with the associated ACL, routing policies and often firewalling)


<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Xjo9UG90EOo" frameborder="0" allowfullscreen></iframe>


## NAT

Network address translation, or NAT, is the mechanism where the internal IP addresses are shielded from the outside, and use the routers external IP address to access servers on the outside network. It is a one-way mechanism, so the outside servers cannot initiate a connection going in.

The above is "source NAT". There are other types as well.

[Port forwards and NAT](https://www.homenethowto.com/ports-and-nat/port-forward/)


### DMZ

The most well-known and old example. The external firewall allows some traffic going in to a specific subnet. This subnet is where web, mail, and other external facing servers are. This is an obvious security risk, and a common solution is to create a subnet specific for the services that must be reachable from the internet.

Often, on smaller networks, this includes making port forwards.

### Subnets in general

The rule of thumb is that a given subnet must have one purpose. 

* DMZ: for external facing servers
* Management: a non-routing network to administer infrastructure devices
* User (multiple): depending on size, wifi user could be in one subnet and cabled users in a different one
* VPN: a subnet for the user that connect from the outside to get access to internal service (and internet access)
* Server (multiple): depending on size and throughput, storage and database server could be in their own subnet.
* Dedicated cable connections: In principle, this is a subnet. Some servers will have a direct connection between them, either for speed or security. A common example is a redundant router/firewall setup, where the internal connection tables are continuously synchronized.

Digital has a piece on [Load balancing design](https://www.digitalocean.com/community/tutorials/5-common-server-setups-for-your-web-application)

An example of an actual network with subnets is the [Bornhack network from 2019](https://bornhack.gitlab.io/noc/2019/hardware/2019/08/12/network-plan.html).

Juniper has a long descriotion on ["metafabric 1.0" implementation](https://www.juniper.net/documentation/en_US/release-independent/nce/topics/solution-design.html). They touch on a lot of interesting technologies and topic, like edge vs. core.

### VLANs

VLANs are defined in [802.1q](https://en.wikipedia.org/wiki/IEEE_802.1Q). It is a way to use the same cables for multiple subnet, without them being able to reach each other on layer 2. The switch handles the tagging and untagging, and most port on a switch will be access port, that only allow one untagged VLAN, so that connected devices are unaware of the fact that VLANS are being used.


### Set up a router and subnet in vmware

Do or redo [this exercise](https://moozer.gitlab.io/course-networking-basisc/05_more_routing/setup/#importing-router-1h) from networking basics.

As an extra exercise, use `wireshark` or `tcpdump` to see the difference between incoming and outgoing packets on the router.

