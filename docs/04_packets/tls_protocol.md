TLS protocol
===============

In most text, you will se TLS and SSL being used interchangeably. This is for historic reasons, but now SSL is obsolete and we should TLS everywhere. For more history on SSL/TLS, see [Feisty duck's timeline](https://www.feistyduck.com/ssl-tls-and-pki-history/)

TLS is in widespread use to encrypt underlying protocols like HTTP, SMTP, DNS and so on. It handles the key exchanges and the following encrypted data transmission.

Cloudflare has a [blogpost](https://www.cloudflare.com/en-gb/learning/ssl/what-happens-in-a-tls-handshake/) about the mechanics of a TLS connection.

From a non-specialist point of view, keywords include

* Certificates and CA authorities: This is foundational, and out-of-scope here. This is cryptographic keys and metadata that use to prove the identity of servers and services.
* TLS versions: Takeaway os to use TLS1.3 where possible and at worst TLS1.2. All previous versions are obsolete.
* Cipher suite: One of the key features of TLS1.3 is that old ciphers are not allowed anymore.


Rerefences

* [sectigostore on TLS1.3](https://sectigostore.com/blog/tls-version-1-3-what-to-know-about-the-latest-tls-version/)
* [sectigostore on certificates](https://sectigo.com/resource-library/what-is-x509-certificate)
* [SSL.com on certificate authorities](https://www.ssl.com/faqs/what-is-a-certificate-authority/#:~:text=A%20certificate%20authority%20(CA)%2C,the%20issuance%20of%20electronic%20documents)
* [Feisty duck's newsletter](https://www.feistyduck.com/bulletproof-tls-newsletter/) is recommended reading.

SSL analysis
----------------------

1. Install `ssldump` either in Kali or similar Linux box
2. Run it from the command line: `ssldump -j -ANH -n -i any | jq .`
3. Generate some HHTPS traffic, e.g. by browsing
3. Evaluate what is shown.

    * How does ssl dump know the domain name? Could we turn that off?
    * When handshaking, the certificate chain is shown. Should that not be secret?