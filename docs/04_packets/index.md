Packets
====================

To look into packets, ie. the blocks of data being transmitted on the wire or wirelessly, we use [tcpdump](https://www.tcpdump.org/), [wireshark](https://www.wireshark.org/) or similar programs.

Most of these programs support both data processing and saving to PCAPS, and also both online and offline work (ie. analysing PCAPS)

For an in depth description of sniffing traffic, see [section 4 of networking basics](https://moozer.gitlab.io/course-networking-basisc/02_OSI/nw_traffic/).


Network analysis and tcpdump
---------------------------

Tcpdump et al are tools to bulk collect and analyse packets. Some are stateless, other are stateful and can process to a certain level. 

There are many tools available to process packet flows and connections. A non-extorsive list is the tools included in [security onion](https://docs.securityonion.net/en/16.04/tools.html).

Most network analysis tool to be able to import PCAPs, and saving PCAPS for later analysis is common. The main issue is that PCAPS takes up comparable space as to the data transmitted on wire, which quickly becomes massive amount of data. Online data processing to extract metadata is widespread and some of them will have built-in logic to save PCAPS on specific conditions.

E.g. Netflow is a protocol that extract lots of connection information to create an overview of network traffic based on the raw packets. If you save data from tcpdump to PCAP, you will be able to process it using ntopng like if it came from an interface. See the [manual](https://www.ntop.org/guides/ntopng/cli_options/cli_options.html?highlight=import%20pcap) for details.

### Collection packets and processing them

1. Use tcpdump to collect a couple of MB of network traffic from any device, and save to pcaps
2. Copy the PCAPs to a desktop
3. Open the PCAPs in wireshark
4. Use `ntopng -f <pcap file>` to load the PCAPs
5. Go to the ntop website and compare result with what you see in wireshark
    * are the data as expected?
    * what extra data does wireshark show?
    * what extra data does ntopng show


References:

* [Rotating PCAPs by F5](https://support.f5.com/csp/article/K65251607)

