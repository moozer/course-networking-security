Kali on VMware
=======================

Kali is a good tool to have, and contains a lot of the tools we need for testing and checking.

Using the Live image is a simple way of ensuring that changes to kali will be erased at reboot. This non-persistence is useful when testing and installing questionable stuff. Also, it is testing the vmware installation and virtualization hardware without using much disk space.

To get Kali linux into your VMware workstation

1. Go to the [kali live download page](https://www.kali.org/get-kali/#kali-live)
2. Download the 64 bit point release

    When downloading large images, torrent is recommended, since it puts a lower load on the download servers. Also consider to leave the torrent running for a while to contribute a bit to Kali.

3. Import the iso by creating a new virtual machine and mount the iso.

    Just use defaults. It will create a hard disk for the virtual machine, but it will not be in use. Also, changing VM config later is trivial.

    Documentation about creating virtual machine is in the [vmware docs](https://kb.vmware.com/s/article/1018415)

4. Start it

    Use ctrt+alt to get your cursor back, it is get stuck in the VM.

5. Verify internet connectivity by starting a shell and issue a ping, e.g. `ping google.com`.


The UI of the VM is limited, and if you want to work more seriously with Kali, it is recommended to make a non-live version, with the proper vmware guest tools installed. There is a guide for that in the [kali docs](https://www.kali.org/docs/virtualization/install-vmware-guest-tools/).