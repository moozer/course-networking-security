# Bonus content

This section contains links and info that doesn't fit with the course, but is used a supporting material.

* [Install VMware workstation](install_vmware.md)
* [Kali on VMware workstation](kali_on_vmware.md)