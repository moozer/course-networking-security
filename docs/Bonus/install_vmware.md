Install vmware workstation
==================================

In this course, we supply some vmware VMs, so having a functioning VMware is relevant.

Note that VMWare workstation is a non-free software, and you must acquire a license to use it.

1. Download the evaluation version from [vmware.com](https://www.vmware.com/dk/products/workstation-pro/workstation-pro-evaluation.html)

    Current as of this wtiting is version 16.2.1. The specific version should not be important.

3. Log in as root, or use sudo (depending on your configuration)
2. Go to the download directory
3. Run the bundle script: `./VMware-Workstation-Full-16.2.1-18811642.x86_64.bundle`
4. After installation, run the program using `./vmware`
5. Go through the wizards and respons to the parts with license agreements, CEIP, and such
6. Type the license key.
7. Click Ok to finalize

    If prompted, supply the root password

8. VMware workstation should now be installed, and you may import/create/administrate your virtual machines.

You may encounter issues with running multiple hypervisors at the same time, e.g. KVM and VMware workstation simultaneously.