Network layer
============================

On layer 3, we have the network layer. This is all about logical connection of devices using [IP addresses](https://www.lifewire.com/what-is-an-ip-address-2625920).

You will end up with a lot of IP subnets ranging from a 2 IP subnet between routers to large networks for users.


Grouping subnets
------------------------------

In principle, any subnet can have any ip address range not already claimed in the network. This is in general a bad strategy for debugging and maintainability.

A better approach is to have recognizable IP addresses for the different parts of the subnet.

E.g.

* 172.16.x.x/12: Management interfaces of router, switches and other infrastructure devices.
* 10.0.0.0/16: Cabled user devices
* 10.1.0.0/16: wireless user devices
* 10.2.0.0/16: VPN users
* 10.10.0.0/16: Globally accessible servers
* 10.20.0.0/16: Internal restricted servers
* 10.250.0.0/16: Reserved for building automation
* 192.168.x.x/16: DMZ/External facing servers

Having a well defined IP scheme will make certain firewall rules much simpler, more readable, and hence often more secure. It is also a very strong debugging tool.

On the other hand having a well-defined IP scheme will make things easier for an adversary, but in general, the advantages in maintainability outweights the disadvantage.


Subnetting (and IP trees)
--------------------

The concept of subnetting arises from when we used classes for the size of subnets. We have since moved to using CIDR notation ([rfc4632](https://tools.ietf.org/html/rfc4632)), but the core idea remains the same.

The basics idea is to take larger IP blocks and subdivide them, and assign smaller and smaller pools of IP addresses to "lower" subnets.

![](ip-plan-tree.png)

This is especially relevant when working with IPv6 where the address space is so large, that having some sort of plan to handle it is mandatory.

Subnetting could be based on geography or the physical layout of the network. This would tie the ip plan to how the routers are connected, e.g. giving a rack an IP range or a building. This is most advantageous if the location talk a lot internally.

In any case, using a logical tree like the one presented will simplify routing tables and probably also firewall rules.

Special subnets and collisions
------------------------------------

When using NAT, the internal IP addresses are hidden from the outside. This does not make the choice of internal IP address range redundant.

If the internal network range matches a network used elsewhere in the network, the other network will not be accessible by the host. This is an issue that mostly arise when using default addresses.

Most virtualization (vmware, libvirt, vagrant) and containerization (docker, LXC) system will create internal NAT network on the host. A normal implementation of this is to have a full hardcoded default range or have a part hardcoded and another part random. In the latter case, you encounter the problem that "it just doesn't work" on some device while other devices with the same config have access. 

Another special subnet is 127.0.0.0/8. This is reserved for localhost access only (ie. "loopback"), and should never be routed.

Other subnet ranges are for [private networks]([rfc 1918](https://tools.ietf.org/html/rfc1918)),  multicasting ([RFC5771](https://datatracker.ietf.org/doc/html/rfc5771)), documentation ([RFC5737](https://datatracker.ietf.org/doc/html/rfc5737)), and other purposes. These are summarized in [RFC5735](https://datatracker.ietf.org/doc/html/rfc5735#section-4)

Managing the IP plan
----------------------------

The concept is called IPAM ("IP Address Management"). It is a system to handle documentationa and overview of subnet, IP addresses and such.

[Netbox](https://netbox.readthedocs.io/en/stable/) is an open source solution for this and they have a [demo site](https://demo.netbox.dev/) to get an idea about it.


References

* [Bluecat on IPAM](https://bluecatnetworks.com/glossary/what-is-ipam/) (Vendor link)
* [OpUtils on IPAM](https://www.manageengine.com/products/oputils/what-is-ipam.html) (Vendor link)


