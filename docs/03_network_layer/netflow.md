Netflow
=================

Netflow is a common tool for collecting informatin about connections going in and out of a network. It is often used forensically to see if, in hindsight, the network has been in contact with bad ip addresses.

Most setup includes probes that run on routers or servers that transmit to a centralized server. This is similar in design to how logs are handled, except that probes are not installed on all devices.

The name "netflow" is the cisco name, and what was used in the old versions. The corresponding RFC is [rfc 7011](https://datatracker.ietf.org/doc/html/rfc7011) and is about "IPFIX".

References:

* [Netflow on wikipedia](https://en.wikipedia.org/wiki/NetFlow)
* [Gigamon on netflow](https://blog.gigamon.com/2018/01/08/what-is-netflow/) (vendor link)
* [youtube from ZCorum on netflow](https://www.youtube.com/watch?v=lebIEzZcAKo) (vendor link)
* [Auvik on netflow](https://www.auvik.com/franklyit/blog/netflow-basics/) (vendor link, but better)


netflow using ntopng
---------------------------

[Ntop](https://www.ntop.org/products/traffic-analysis/ntop/) is an open source implementation of netflow. It is the concentrator with an appropriate web interface. In the same family is nprobe, which is the probe to be installed on e.g. a router.

1. On a debian VM, install ntopng
2. Go to the website to see the connections (default is localhost port 3000)
3. Generate more traffic by visiting sites.

Note: We ought to set up a probe on the router, but due to a technical issue, this is postponed.
